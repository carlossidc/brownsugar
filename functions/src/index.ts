import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';


import {dialogflow, Image} from 'actions-on-google';

// Create an app instance with all the tools and capabilities presented in teh dialogflow sdk
const app = dialogflow({debug: true});

//create the dialogflow fullfillment function using the app instance
export const dialogflowFirebaseFulfillment = functions.https.onRequest((app));

//initialize the firebase functions
admin.initializeApp();


//Variables that I use in my code
const words = ['mom', 'dad', 'sister', 'brother', 'perrin'];

/*
*
*                      El Mogollon empieza aqui
*
* */


app.intent<{ word: string }>('wordGame', (conv, {word}) => {


    conv.ask('your word is -----> ' + word);

});

/*
*
*                   Perfectly Working Intents, do not fuck here plz
*
* */


//i will save 5 words, tell me a number from 1 to 5 and i will tell you the corresponding word
app.intent<{ nomber: number }>('dimeUnNumero', (conv, {nomber}) => {

    if (nomber == 0) {
        conv.ask('ese numero no se vale, tiene que ser del 1 al 5')
    }
    else if (nomber <= 5) {
        const nomnom = nomber - 1;
        const palabra = words[nomnom];
        conv.ask('tu palabra es ----> ' + palabra);
    }
    else {
        conv.ask('te mamaste perro de dije que del 1 al 5');
    }
});

//Default welcome intent
app.intent('Default Welcome Intent', conv => {
    conv.ask('Hi, how is it going? Here\'s a picture of a friendly cat');
    //conv.ask(`Here's a picture of a friendly cat`);
    conv.ask(new Image({
        url: 'https://developers.google.com/web/fundamentals/accessibility/semantics-builtin/imgs/160204193356-01-cat-500.jpg',
        alt: 'A cat',
    }));
});

// Intent in Dialogflow called Goodbye
app.intent('Goodbye', conv => {
    conv.close('See you later amigo!')
});

//intent in dialogfrlow called palabraMuyLarga
app.intent<{ word: string }>('palabraMuyLarga', (conv, {word}) => {

    const dimensionss = word.length;
    if (dimensionss >= 4) {
        conv.ask('palabra de mas de 4 letras amigo ' + word);
    }
    else {
        conv.ask('palabra de menos de 4 letras amigo! ' + word)
    }

});

//intent in dialogflow called default fallback intent
app.intent('Default Fallback Intent', conv => {
    conv.ask(`I didn't understand. Can you tell me something else pinche amigo?`)
});


/**
 * Esta funcion recibe un arreglo como el siguiente:
 * {"words":["dog","car","yellow"]}
 */

export const addWords = functions.https.onRequest(async (request, response) => {
    try {
        let arrWords = request.body.words;
        var addedWords = 0;
        var notAddedWords = 0;

        //Iterate over the word array
        for (let word of arrWords) {


            //---> Yayo Comment --- >Get the existing words
            //it takes the word that you put in the jason, then check the collection for that word,

            let queryWord = await admin.firestore().collection("words")
                .where("word", "==", word).get();
            
            //if the word its been added before to the collection, there will be a "value" in queryWord
            //therefore there will be a "size" property, which in this case we will use as the variable to apply
            //the === operator

            //----> Yayo comment --->  If the query size is not 0 it means the word already exists.
            if (queryWord.size === 0) {
                //Create a new document in the "words" collection
                let refWords = admin.firestore().collection("words").doc();

                //Write in the database the words
                await refWords.set({
                    id: refWords.id,
                    word: word,
                    startLetter: word.charAt(0),
                    endLetter: word.charAt(word.length - 1)
                });
                console.log(`La palabra agregada es: ${word}`);
                addedWords++;
            } else {
                console.log(`La palabra que no se agrego es: ${word}`);
                notAddedWords++;
            }
        }

        response.status(200).json({
            message: `Success`,
            added: addedWords,
            notAdded: notAddedWords
        })
    }
    catch (e) {
        console.log(e)
    }
});